#!/usr/bin/env python3

import requests
import pickle
import time
import argparse

from utils.pdi_search_utils import search_csv, search_by_precincts, PDI_URL

# Simple script to login to PDI and perform a search
# Ex usage:
#     ./pdi-search.py --username <user> --password <password> --firstname aaron --lastname clark
#
# TODO:
# * Add CSV support
# * Don't login every run / clean that up
# * Map CSV colums to search parameters
# * Create Different exception types
# * Probably tons more

PDI_LOGIN = PDI_URL + "/PDI"

session = requests.Session()


# POST Login request to PDI, store session in local cookie file
def login(username, password):
    login_request_payload = {
        "EulaChecked": True,
        "FingerPrint": "3264401965",
        "UserName": username,
        "Password": password,
        "TimeZoneOffset": 0
    }
    session.post(PDI_LOGIN, login_request_payload)
    with open("./cookiefile", 'wb') as f:
        pickle.dump(session.cookies, f)


# Load local cookie file
def load_cookie():
    try:
        with open("./cookiefile", 'rb') as f:
            session.cookies.update(pickle.load(f))
    except:
        print("Please login")
        exit(1)


# TODO: Redo this?
# CLI
'''
example input: 
    python pdi-search.py 
    --search_columns first_name,last_name,email  
    --input_csv input_file.csv 
    --username ali.martin.sakr@gmail.com 
    --password [your pdi/onlinecampaigntools password] 
    --field_to_fill zip 
    --output_csv output_file.csv

'''
t1 = time.time()
parser = argparse.ArgumentParser()

parser.add_argument('--username')
parser.add_argument('--password')
parser.add_argument('--input_csv')
parser.add_argument('--search_columns')
parser.add_argument('--output_csv')
parser.add_argument('--field_to_fill')
parser.add_argument('--precinct_input')

args = parser.parse_args()
parsed_args = vars(args)
login(parsed_args["username"], parsed_args["password"])
load_cookie()
if not parsed_args['precinct_input'] or parsed_args['precinct_input'] == '' or parsed_args['precinct_input'] is None:
    search_columns = parsed_args['search_columns'].split(',')
    output_csv = parsed_args['output_csv']
    field_to_get = parsed_args['field_to_fill']
    print(search_columns)
    search_csv(session, parsed_args['input_csv'], search_columns, field_to_get, output_csv)
else:
    search_by_precincts(parsed_args['precinct_input'], session)
t2 = time.time()
run_time = t2-t1
minutes = int(run_time / 60)
seconds = run_time % 60
print('total time was {} minutes and {} seconds'.format(minutes, seconds))
