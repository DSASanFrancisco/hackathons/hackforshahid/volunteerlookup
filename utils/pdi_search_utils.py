import json
import pandas as pd
import re
# import numpy as np

PDI_URL = "https://onlinecampaigntools.com"
PDI_SEARCH = PDI_URL + "/Lookup/GetSearchRecords"
PDI_EMAIL_FIELD = 'vl_txtEmailAddress'
PDI_FIRST_NAME_FIELD = 'vl_txtFname'
PDI_LAST_NAME_FIELD = 'vl_txtLname'
PDI_ZIP_FIELD = 'vl_txtZip'
PDI_PHONE_NUMBER = 'vl_txtPhoneNumber'
PDI_PRECINCT = 'vl_txtPrecinct'


def get_pdi_search_key(pdi_field):
    return 'searchParams[{}]'.format(pdi_field)


def get_pdi_result_field(csv_field):
    csv_to_pdi_map = {
        'zip': 'address',
        'email': 'email',
        'phone': 'phone',
        'address': 'address'
    }
    '''
        TODO: add search for name, would require adding below into map
        'first_name': 'fullname',
        'last_name': 'fullname',
    '''
    if csv_field not in csv_to_pdi_map:
        error_message = 'cannot fill "{}" field, select one of the following {}'
        error_message.format(csv_field, csv_to_pdi_map.keys())
        raise Exception(error_message)
    return csv_to_pdi_map[csv_field]


def get_pdi_field_name(csv_field):
    if csv_field.startswith('alternate_'):
        csv_field = csv_field[10:]
    csv_to_pdi_map = {
        'zip': PDI_ZIP_FIELD,
        'email': PDI_EMAIL_FIELD,
        'first_name': PDI_FIRST_NAME_FIELD,
        'last_name': PDI_LAST_NAME_FIELD,
        'phone': PDI_PHONE_NUMBER,
        'precinct': PDI_PRECINCT
    }
    if csv_field not in csv_to_pdi_map:
        error_message = 'cannot search by "{}", please search by one or more of the following {}'
        error_message.format(csv_field, csv_to_pdi_map.keys())
        raise Exception(error_message)
    return csv_to_pdi_map[csv_field]


def parse_user_record(user_record, field_to_get):
    result = dict((k.lower(), user_record[k]) for k in user_record)
    output_field = get_pdi_result_field(field_to_get)
    if output_field in result and len(result[output_field]) > 0:
        if field_to_get == PDI_ZIP_FIELD:
            address_parts = result[output_field].split()
            zip_code = address_parts[len(address_parts) - 1]
            if len(zip_code) == 5 and zip_code.isnumeric():
                return zip_code
        else:
            return result[output_field]
        # TODO: here we are either filling the first or last name
    return ''


def search_by_precincts(csv_file, pdi_session):
    df = pd.read_csv(csv_file)
    result_df = None
    i = 0
    for row in df.iterrows():
        map_row = get_column_value_map_of_df_row(row)
        if map_row['In_District'] == 'No':
            continue
        precinct = 380000 + map_row['precinct']
        result_df = search_by_precinct(precinct, pdi_session, result_df)
        if i < 5:
            print('dataframe now has {} rows'.format(len(result_df)))
        i += 1

    print('dataframe now has {} rows'.format(len(result_df)))
    result_df.to_csv('voters_by_precinct.csv', index=False)


def search_by_precinct(precinct, pdi_session, df):
    pdi_field = get_pdi_field_name('precinct')
    search_key = get_pdi_search_key(pdi_field)
    search_payload = dict()
    search_payload[search_key] = precinct
    r = pdi_session.post(PDI_SEARCH, search_payload)
    response_json = json.loads(r.text)
    num_records = response_json['records']
    if num_records >= 2000:
        print('got {} voters in precinct={}'.format(num_records, precinct))
    if num_records == 0:
        print('no voters in precinct={}'.format(precinct))
        return df
    if df is None:
        return pd.DataFrame(response_json['rows'])
    df = df.append(response_json['rows'])
    return df



# POST Search request to PDI
def search(pdi_session, row_map, search_columns, field_to_get, searched_map):
    # Optional search fields:
    # searchParams[searchtype]: 1
    # searchParams[vl_txtLname]:
    # searchParams[vl_txtFname]:
    # searchParams[vl_txtMname]:
    # searchParams[vl_txtRange]:
    # searchParams[vl_txtStreetName]:
    # searchParams[vl_txtRACity]:
    # searchParams[vl_txtZip]:
    # searchParams[vl_txtEmailAddress]:
    # searchParams[vl_ListParty]:
    # searchParams[vl_txtPhoneNumber]:
    # searchParams[vl_txtBirthDate]:
    # searchParams[vl_txtPrecinct]:
    # searchParams[vl_ListCounty]:
    # searchParams[vl_txtPrimID]:
    # searchParams[vl_ListUniverseID]: -1
    # searchParams[vl_ListUniverseIDContact]: -1
    # searchParams[AssociateName]:
    # searchParams[IssueName]:
    # searchParams[CategoryName]:
    # searchParams[questionName]:
    # searchParams[codeName]:
    # searchParams[sourceName]:
    # _search: false
    # nd: 1575757994287
    # rows: 500
    # page: 1
    # sidx:
    # sord: asc
    search_payload = dict()
    search_key = ''
    for input_column in search_columns:
        if input_column not in row_map or len(row_map[input_column]) == 0:
            alt_input = 'alternate_' + input_column
            if alt_input not in row_map or len(row_map[alt_input]) == 0:
                continue
            else:
                input_column = alt_input
        pdi_field = get_pdi_field_name(input_column)
        if input_column == 'zip' or input_column == 'alternate_zip':
            row_map[input_column] = str(row_map[input_column]).strip()
            row_map[input_column] = row_map[input_column][:5]
        full_field_name_in_post = get_pdi_search_key(pdi_field)
        if full_field_name_in_post not in search_payload:
            search_payload[full_field_name_in_post] = row_map[input_column]
            search_key += row_map[input_column] + '_'
    if search_key in searched_map:
        print("already searched this")
        if searched_map[search_key] is False:
            raise Exception("already seen and failed")
        print("writing duplicate")
        return searched_map[search_key]
    r = pdi_session.post(PDI_SEARCH, search_payload)
    try:
        response_json = json.loads(r.text)
    except Exception as e:
        print("not json {}".format(r.text))
        searched_map[search_key] = False
        message = 'Couldn\'t parse response to JSON, verify username, password ' \
                  'and post_columns are correct exception:{}'
        raise Exception(message.format(str(e)))
    num_records = response_json['records']
    if num_records == 0:
        searched_map[search_key] = False
        raise Exception("No records found")
    if num_records > 1:
        searched_map[search_key] = False
        print("found {} duplicates {}".format(num_records, search_payload))
        raise Exception("Search obtained {} people rather than a unique person".format(num_records))
    voter_record = response_json['rows'][0]
    result = parse_user_record(voter_record, field_to_get)
    if result == '':
        searched_map[search_key] = False
    else:
        searched_map[search_key] = result
    return result


def get_column_value_map_of_df_row(row):
    return row[1].to_dict()


def get_raw_field_value(field_to_get, map_row):
    if field_to_get not in map_row or map_row[field_to_get] is None or len(str(map_row[field_to_get])) == 0:
        return ''
    val = str(map_row[field_to_get])
    val = val.strip()
    return val


def has_valid_zip(map_row):
    val = get_raw_field_value('zip', map_row)
    val = re.sub('[^0-9]', '', val)
    if len(val) == 5:
        return True
    val = get_raw_field_value('alternate_zip', map_row)
    val = re.sub('[^0-9]', '', val)
    return len(val) == 5


def has_valid_phone(map_row):
    val = get_raw_field_value('phone', map_row)
    val_nums_only = re.sub('[^0-9]', '', val)
    # assume that if there are more than 10
    # digits in the number that it starts with a country code
    # and that we can use the last 10 digits as the phone number
    if len(val_nums_only) >= 10:
        return True
    val = get_raw_field_value('alternate_phone', map_row)
    val_nums_only = re.sub('[^0-9]', '', val)
    return len(val_nums_only) >= 10


def has_valid_email(map_row):
    val = get_raw_field_value('email', map_row)
    if len(val) > 0 and (('@' in val and '.edu' in val) or ('@gmail' in val) or ('@yahoo' in val)):
        return True
    val = get_raw_field_value('alternate_email', map_row)
    return len(val) > 0 and (('@' in val and '.edu' in val) or ('@gmail' in val) or ('@yahoo' in val))


def has_valid_address(map_row):
    val = get_raw_field_value('address', map_row)
    val = re.sub('[^0-9]', '', val)
    if len(val) > 0:
        return True
    val = get_raw_field_value('alternate_address', map_row)
    val = re.sub('[^0-9]', '', val)
    return len(val) > 0


def is_value_valid_and_filled(field_to_get, map_row):
    if field_to_get == 'zip':
        return has_valid_zip(map_row)
    if field_to_get == 'phone':
        return has_valid_phone(map_row)
    if field_to_get == 'email':
        return has_valid_email(map_row)
    if field_to_get == 'address':
        return has_valid_address(map_row)
    raise Exception('cannot currently attempt to fill {}'.format(field_to_get))


def search_csv(pdi_session, csv_file, search_columns, field_to_get, output_csv):
    df = pd.read_csv(csv_file, dtype={'alternate_zip': str, 'zip': str})
    column_names = list(df.columns.values)
    field_to_populate = 'alternate_' + field_to_get
    if field_to_populate not in column_names:
        df[field_to_populate] = ''
    i = 0
    num_irregular_zips = 0
    num_found = 0
    num_failed = 0
    num_found_zip = 0
    num_new = 0
    num_dups = 0

    searched_map = dict()
    for row in df.iterrows():
        map_row = get_column_value_map_of_df_row(row)
        if not is_value_valid_and_filled(field_to_get, map_row):
            try:
                result = search(pdi_session, map_row, search_columns, field_to_get, searched_map)
                num_found += 1
                if result != '':
                    num_found_zip += 1
                    num_new += 1
                    df.set_value(i, field_to_populate, result)

            except Exception as e:
                if 'rather than a unique person' in str(e):
                    num_dups += 1
                num_failed += 1
        i += 1
        if i % 150 == 0:
            if num_new > 0:
                df.to_csv(output_csv, index=False)
            num_new = 0
            print('total rows is {}'.format(i))
            print('num fields to fill is {}'.format(num_irregular_zips))
            print('num users found is {}'.format(num_found))
            print('num fillable with result {}'.format(num_found_zip))
            print('num threw exception is {}'.format(num_failed))
            print('num dups {}'.format(num_dups))
    if num_new > 0:
        df.to_csv(output_csv, index=False)
    print('total rows is {}'.format(i))
    print('num fields to fill is {}'.format(num_irregular_zips))
    print('num users found is {}'.format(num_found))
    print('num fillable with result {}'.format(num_found_zip))
    print('num threw exception is {}'.format(num_failed))
    print('num duplicates was {}'.format(num_dups))
