import pandas as pd

# to be run from volunteerlookup directory
# python scripts/remove_spaces_from_column_names.py
if __name__ == '__main__':
    df = pd.read_csv('data/master_sheet_w_inactive_and_active_Dec4.csv')
    print(df.columns)
    df.columns = df.columns.str.replace(' ', '_')
    df.columns = df.columns.str.replace('\?', '')
    df.columns = df.columns.str.replace('/', '_')
    df.columns = df.columns.str.lower()
    df = df.rename(columns={'alternate_emaiil': 'alternate_email', 'date_contcted': 'date_contacted'})
    print(df.columns)
    df.to_csv('data/master_sheet_w_inactive_and_active_Dec4.csv', index=False)


